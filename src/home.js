// function Home()
// {
//     return <h1>Welcome Home</h1>;
// }

import { Component } from "react";
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Something from "./something";
class Home extends Component
{
    constructor()
    {
        super();
        console.log("Home object is created...")
    }
    state = {name: "Sunbeam", city: "Pune"}
  
    // DoSomething=()=>
    // {
    //     console.log("U clicked a button!!!");
    //     this.setState( {name: "CDAC"});
    //     console.log("State Value Changed due to button click...");
    // }

    componentDidMount(){
        console.log("Component Did Mount Method Called!!")
    }

    componentDidUpdate()
    {
        console.log("Component Did Update Method Called!!")
    }

    render()
    {
        console.log("Home - render function called..")
        //debugger;
        return <>
                    <h1>Welcome to bus ticket booking</h1>
                    
                    {/* <Something name={this.state.name} 
                               city = {this.state.city}/>
                    <button className="btn btn-primary" 
                            onClick={this.DoSomething}>Click Me</button> */}
               </>
    }
}

export default Home;