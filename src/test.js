import React, { Component } from 'react';

class Test extends Component {
    state = { emp: {company : "Sunbeam", address: "Pune" }} 

    DoSomething=()=>
    {
        var newEmp  = {...this.state.emp}; //here you are making exact replica of this.state.emp as a fresh new object

        newEmp.company = "Infosys";

        this.setState({emp: newEmp})
    }

    OnTextChange=(args)=>
    { 
        debugger;
        var copyOfEmp  = {...this.state.emp};

        copyOfEmp[args.target.name] =  args.target.value;

        this.setState({emp: copyOfEmp})
        
        
    }
    render() { 
        return (<div>
                  <center>
                    <input type="text" 
                           value={this.state.emp.company}
                           name='company' 
                           onChange={this.OnTextChange} /> 
                           <br/><br/>

                    <input type="text" 
                           value={this.state.emp.address}
                           name='address' 
                           onChange={this.OnTextChange} /> 
                           <br/><br/>

                    <button onClick={this.DoSomething}>Click Me</button>
                  </center>
                </div>);
    }
}
 
export default Test;

