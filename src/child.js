import React, { Component } from 'react';   

class Child extends Component 
{
    constructor(props)
    {
        debugger;
        super(props);
    }
    // DoSomething=()=>{
    //     this.props.myName = "Changed"; //this line throws error!!!
                                          // props is read only
    // }
    render() 
    { 
        return (<>
                    <hr></hr>
                    <h1>This is child</h1>
                    <h1>Welcome to {this.props.myName}</h1>
                    <button onClick={this.props.DoSomething1}>Click Me</button>
                </>);
    }
}
 
export default Child;