import {useEffect, useState } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import RecordAddition from './RecordAddition';

function Dashboard() 
{
    var [buses, setBuses] = useState([]);
    var [message, setMessage] = useState( "");
    var [bus, setBus] = useState({bus_id:0, bus_name: "", bus_number: "", capacity: ""});
    var [operation, setOpration] = useState("");
    var [searchText, setSearchText] = useState("");
   

    useEffect(()=>{
        Select();
    }, []);
    
    var Select = function()
    {
        debugger;
        var helper = new XMLHttpRequest();
        helper.onreadystatechange = ()=>{
            if(helper.readyState == 4 && helper.status == 200)
            {
                var result = JSON.parse(helper.responseText);
                setBuses(result);
            }
        }
        helper.open("GET", "http://localhost:9999/bus/getAllbus");
        helper.send();
    }

    var ShowMessage = function(msg)
    {
        setMessage(msg);
        setTimeout(() => 
                        {
                            setMessage("");
                        }, 5000);
                    }

    var Delete = function (bus_id)
    {
        var helper = new XMLHttpRequest();
        helper.onreadystatechange = ()=>{
            if(helper.readyState == 4 && helper.status == 200)
            {
                var result = JSON.parse(helper.responseText);
                if(result.affectedRows > 0)
                {
                    setMessage("Record removed successfully!!");
                    Select()
                }
                
            }
        }
        helper.open("DELETE", "http://localhost:9999/bus/" + bus_id);
        helper.send();
    }

    var TextChanged=function (args)
    {
        debugger;
        var copyOfBus = {...bus};
        copyOfBus[args.target.name] = args.target.value;
        setBus(copyOfBus);
    }

    var Insert = function()
    {
        if(operation == "edit")
        {
            setMessage('Complete the Edit Operation First!')
        }
        else
        {
              // this.state.emp --this holds data to send to server for Insert!
            var helper = new XMLHttpRequest();
            helper.onreadystatechange = ()=>{
                
                if(helper.readyState == 4 && helper.status == 200)
                {
                    debugger;
                    var result = JSON.parse(helper.responseText);
                    if(result.affectedRows > 0)
                    {
                        setMessage("Bus Added successfully!!");
                        Select();
                        setBus({bus_id: 0, bus_name: "", bus_number: "", capacity: ""});
                    }
                    
                }
            }
                    
        debugger;
        helper.open("POST", "http://localhost:9999/bus");

        helper.setRequestHeader("Content-Type", "application/json");

        var dataToBePassedInStringFormat = JSON.stringify(bus);
        helper.send(dataToBePassedInStringFormat);
        }
    }

    var Edit= function(bus_id){
        for(var i=0; i< buses.length; i++)
        {
            
            if(buses[i].bus_id == bus_id)
            {
                var copyOfBus = {...buses[i]};
                setBus(copyOfBus);
                setOpration("edit");
                break;
            }
        }
    }

    var Update =function(){
        //Here this.state.emp will have updated record.
        //use this to send data using Put request to the server

        var helper = new XMLHttpRequest();
        helper.onreadystatechange = ()=>{
            
            if(helper.readyState == 4 && helper.status == 200)
            {
                debugger;
                var result = JSON.parse(helper.responseText);
                if(result.affectedRows > 0)
                {
                    setMessage("Record Updated successfully!!");

                    Select();
                    
                    setBus({bus_id: 0, bus_name: "", bus_number: "",capacity: ""})
                }
                
            }
        }

        debugger;
        helper.open("PUT", "http://localhost:9999/bus/"+ bus.bus_id);

        helper.setRequestHeader("Content-Type", "application/json");

        var dataToBePassedInStringFormat = JSON.stringify(bus);
        helper.send(dataToBePassedInStringFormat);
    }

    var CancelUpdate = function()
    {
        setOpration("");
        setBus({bus_id: 0, bus_name: "", bus_number: "", capacity:""});
    }

    var Search=function(args)
    {
        console.log("U searched " +  args.target.value);
        setSearchText(args.target.value);
    }
    
    return (<>
    
            <center>
                <RecordAddition bus={bus} 
                                TextChanged={TextChanged}
                                Insert = {Insert}
                                Update = {Update}
                                CancelUpdate = {CancelUpdate}/>
                <h3 className='alert alert-success'>
                    {message}
                </h3>
                <div>
                    Search By Name: {" "}
                    <input type='text' value={searchText}
                            onChange={Search}/>
                </div>
                <br/>
                <div className='table-responsive'>
                
                    <table className='table table-bordered'>
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>NUMBER</th>
                        <th>CAPACITY</th>
                        <th>Delete Here</th>
                    </thead>
                    <tbody>
                        {
                            buses.map((bus)=>{
                                if(searchText=="")
                                {
                                return <tr>
                                <td>{bus.bus_id}</td>
                                <td>{bus.bus_name}</td>
                                <td>{bus.bus_number}</td>
                                <td>{bus.capacity}</td>
                                <td>
                                <button className='btn btn-danger' 
                                onClick={()=>{
                                        Delete(bus.bus_id)
                                    }}>
                                    Delete
                                </button>
                                
                                </td>

                                <td>
                                <button className='btn btn-info' 
                                onClick={()=>{
                                        Edit(bus.bus_id)
                                    }}>
                                    Edit
                                </button>
                                
                                </td>
                            </tr>
                                }
                                else
                                {
                                if(bus.bus_name.toLowerCase().includes(searchText.toLocaleLowerCase()))
                                {
                                    return <tr>
                                    <td>{bus.bus_id}</td>
                                    <td>{bus.bus_name}</td>
                                    <td>{bus.bus_number}</td>
                                    <td>{bus.capacity}</td>
                                    <td>
                                    <button className='btn btn-danger' 
                                    onClick={()=>{
                                            Delete(bus.bus_id)
                                        }}>
                                        Delete
                                    </button>
                                    
                                    </td>

                                    <td>
                                    <button className='btn btn-info' 
                                    onClick={()=>{
                                            Edit(bus.bus_id)
                                        }}>
                                        Edit
                                    </button>
                                    
                                    </td>
                                </tr>
                                }
                                }
                            })

                        }
                    </tbody>
                    </table>
                </div>
            </center>
        </>);
    }
 
export default Dashboard;