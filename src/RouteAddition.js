function RouteAddition(props)
{
        return (<>
                 <table>
                    <tbody>
                        
                    <center>
                        <tr>
                            <td>Route Name</td>
                            <td>
                                <input type="text" 
                                        name="route_name"
                                        value={props.route.route_name}onChange={props.TextChanged}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Start Location</td>
                            <td>
                                <input type="text" 
                                        name="start_location"
                                        value=
                                        {props.route.start_location}
                                        onChange={props.TextChanged}/>
                            </td>
                        
                            <td>End Location</td>
                            <td>
                                <input type="text" 
                                        name="end_location"
                                        value=
                                        {props.route.end_location}
                                        onChange={props.TextChanged}/>
                            </td>
                            <td>Fare</td>
                            <td>
                                <input type="number" 
                                        name="fare_amount"
                                        value=
                                        {props.route.fare_amount}
                                        onChange={props.TextChanged}/>
                            </td>

                         </tr>   
                        
                        <tr>
                            <td>
                                <button className='btn btn-success' onClick={props.Insert}>
                                    Add Bus
                                </button>
                            </td>
                            <td style={{padding: 10}}>
                                
                                <button className='btn btn-warning' onClick={props.Update}>
                                    Update Bus
                                </button>
                                {"    "}
                                <button className='btn btn-warning' onClick={props.CancelUpdate}>
                                    Cancel Bus Update
                                </button>
                            </td>
                        </tr>
                        </center>
                    </tbody>
                 </table>
                </>);
}

 
export default RouteAddition;