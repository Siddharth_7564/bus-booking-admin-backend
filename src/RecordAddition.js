function RecordAddition(props)
{
        return (<>
                 <table>
                    <tbody>
                        {/* <tr>
                            <td>ID</td>
                            <td>
                                <input type="number" 
                                        name="bus_id"
                                        value={props.bus.bus_id}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr> */}

                        <tr>
                            <td>NAME</td>
                            <td>
                                <input type="text" 
                                        name="bus_name"
                                        value={props.bus.bus_name}onChange={props.TextChanged}/>
                            </td>
                        </tr>

                        <tr>
                            <td>NUMBER</td>
                            <td>
                                <input type="text" 
                                        name="bus_number"
                                        value=
                                        {props.bus.bus_number}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr>
                        <tr>
                            <td>CAPACITY</td>
                            <td>
                                <input type="number" 
                                        name="capacity"
                                        value=
                                        {props.bus.capacity}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <button className='btn btn-success' onClick={props.Insert}>
                                    Add Bus
                                </button>
                            </td>
                            <td style={{padding: 10}}>
                                
                                <button className='btn btn-warning' onClick={props.Update}>
                                    Update Bus
                                </button>
                                {"    "}
                                <button className='btn btn-warning' onClick={props.CancelUpdate}>
                                    Cancel Bus Update
                                </button>
                            </td>
                        </tr>
                    </tbody>
                 </table>
                </>);
}

 
export default RecordAddition;