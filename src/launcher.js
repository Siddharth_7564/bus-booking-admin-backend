import { Link,Switch,Route } from "react-router-dom";
import Footer from "./footer";
import Header from "./header";
import Home from "./home";
import Dashboard from "./dashboard";
import NotFound from "./NotFound";
import About from './about';
import Contact from './contact';
import ProtectedRoute from "./ProtectedRoute";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Login from "./Login";
import Profile from "./profile";
import NormalRoute from "./NormalRoute";


function Launcher()
{
    var [user, setUser] = useState("");
    var history = useHistory();
    
    useEffect(()=>{
        debugger;
        var username = sessionStorage.getItem('username');
        if(username!=null && username !="")
        {
                setUser(username)
        }
        else
        {
                setUser("Guest");
        }
    }, [])

    var SignOut = function(){
        debugger;
        sessionStorage.removeItem("isUserLoggedIn");
        sessionStorage.removeItem("username");
        history.push('/home');
    }

    var GetButton = function()
    {
        debugger;
        var isUserLoggedIn = sessionStorage.getItem("isUserLoggedIn");
        if(isUserLoggedIn!=null)
        {
         return (  
                <button className="btn btn-danger" onClick={SignOut}>
                        Log out
                </button>)   
        }
        else
        {
                return null;
        }
    }

    debugger;
    return <>
            <Header></Header>
            <hr></hr>
              <div style={{padding: 20}}>
                <Link to="/home">Home</Link> | {"  "}
                <Link to="/about">Add Routes</Link> | {"  "}
                <Link to="/contact">Manage Journey</Link> | {"  "}
                <Link to="/db">Add Bus</Link> | {"  "}
                <Link to="/profile">Profile</Link> | {"  "}
                <Link to="/login">Login</Link> | {"  "}
              </div>
            <hr></hr>
          
            <label style={{float: "right", marginRight: 200}}>
                Welcome {user} ! {GetButton()}
            </label>
            <br></br>
            <hr></hr>
                <Switch>
                    <NormalRoute exact path="/home" 
                                 component={Home} />
                    {/* <NormalRoute exact path="/about" 
                                 component={About} /> */}
                     <ProtectedRoute path="/about" 
                            component={About}
                            setUser={setUser}/>            
             
                    <NormalRoute exact path="/contact" 
                                 component={Contact} />
                    {/* <Route  exact path="/login" 
                            component={Login}
                            /> */}
                    <NormalRoute exact path="/login" 
                                 component={Login}
                                 setUser={setUser}/>
                    <ProtectedRoute path="/db" 
                            component={Dashboard}
                            setUser={setUser}/>
                    <ProtectedRoute path="/profile" 
                            component={Profile}
                            setUser={setUser}/>
                    {/* <ProtectedRoute path="/changePassword" 
                            component={ChangePassword}/> */}
                    <Route exact path="/" 
                            component={Home} />
                    <Route  path="*" 
                            component={NotFound} />
                </Switch>
            <hr></hr>
            <Footer></Footer>
           </>
}

export default Launcher;