import {useEffect, useState } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import ProfileShow from './ProfileShow';

function Profile()
{
    var [users, setUsers] = useState([]);
    var [message, setMessage] = useState( "");
    var [user, setUser] = useState({user_id:0, username: "", email: "", mobileno: "",  age:0, gender:"",  user_type:"" });
    var [operation, setOpration] = useState("");
    var [searchText, setSearchText] = useState("");
  

    useEffect(()=>{
        Select();
    }, []);
    
    var Select = function()
    {
        debugger;
        var helper = new XMLHttpRequest();
        helper.onreadystatechange = ()=>{
            if(helper.readyState == 4 && helper.status == 200)
            {
                var result = JSON.parse(helper.responseText);
                setUsers(result);
            }
        }
        helper.open("GET", "http://localhost:9999/user");
        helper.send();
    }

    var ShowMessage = function(msg)
    {
        setMessage(msg);
        setTimeout(() => 
                        {
                            setMessage("");
                        }, 5000);
                    }

    var Delete = function (user_id)
    {
        var helper = new XMLHttpRequest();
        helper.onreadystatechange = ()=>{
            if(helper.readyState == 4 && helper.status == 200)
            {
                var result = JSON.parse(helper.responseText);
                if(result.affectedRows > 0)
                {
                    setMessage("Record removed successfully!!");
                    Select()
                }
                
            }
        }
        helper.open("DELETE", "http://localhost:9999/user/" + user_id);
        helper.send();
    }

    var TextChanged=function (args)
    {
        debugger;
        var copyOfUser = {...user};
        copyOfUser[args.target.name] = args.target.value;
        setUser(copyOfUser);
    }

    var Insert = function()
    {
        if(operation == "edit")
        {
            setMessage('Complete the Edit Operation First!')
        }
        else
        {
              // this.state.emp --this holds data to send to server for Insert!
            var helper = new XMLHttpRequest();
            helper.onreadystatechange = ()=>{
                
                if(helper.readyState == 4 && helper.status == 200)
                {
                    debugger;
                    var result = JSON.parse(helper.responseText);
                    if(result.affectedRows > 0)
                    {
                        setMessage("User Added successfully!!");
                        Select();
                        setUser({user_id: 0, username: "", email: "", mobileno: "",age:0, gender:"", user_type:""});
                    }
                    
                }
            }
                    
        debugger;
        helper.open("POST", "http://localhost:9999/user");

        helper.setRequestHeader("Content-Type", "application/json");

        var dataToBePassedInStringFormat = JSON.stringify(user);
        helper.send(dataToBePassedInStringFormat);
        }
    }

    var Edit= function(user_id){
        for(var i=0; i< users.length; i++)
        {
            
            if(users[i].user_id == user_id)
            {
                var copyOfUser = {...users[i]};
                setUser(copyOfUser);
                setOpration("edit");
                break;
            }
        }
    }

    var Update =function(){
        //Here this.state.emp will have updated record.
        //use this to send data using Put request to the server

        var helper = new XMLHttpRequest();
        helper.onreadystatechange = ()=>{
            
            if(helper.readyState == 4 && helper.status == 200)
            {
                debugger;
                var result = JSON.parse(helper.responseText);
                if(result.affectedRows > 0)
                {
                    setMessage("Record Updated successfully!!");

                    Select();
                    
                    setUser({user_id: 0, username: "", email: "", mobileno: "",age:0, gender:"", user_type:""});
                }
                
            }
        }

        debugger;
        helper.open("PUT", "http://localhost:9999/user/"+ user.user_id);

        helper.setRequestHeader("Content-Type", "application/json");

        var dataToBePassedInStringFormat = JSON.stringify(user);
        helper.send(dataToBePassedInStringFormat);
    }

    var CancelUpdate = function()
    {
        setOpration("");
        setUser({user_id: 0, username: "", email: "", mobileno: "",age:0, gender:"", user_type:""});
    }

    var Search=function(args)
    {
        console.log("U searched " +  args.target.value);
        setSearchText(args.target.value);
    }
    
    return (<>
    
            <center>
                <ProfileShow user={user} 
                                TextChanged={TextChanged}
                                Insert = {Insert}
                                Update = {Update}
                                CancelUpdate = {CancelUpdate}/>
                <h3 className='alert alert-success'>
                    {message}
                </h3>
                <br/>
                <div className='table-responsive'>
                
                    <table className='table table-bordered'>
                    <thead>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>MobileNo</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>User_Type</th>
                        <th>Delete Here</th>
                    </thead>
                    <tbody>
                        {
                            users.map((user)=>{
                                if(searchText=="")
                                {
                                return <tr>
                                <td>{user.user_id}</td>
                                <td>{user.username}</td>
                                <td>{user.email}</td>
                                <td>{user.mobileno}</td>
                                <td>{user.age}</td>
                                <td>{user.gender}</td>
                                <td>{user.user_type}</td>
                                <td>
                                <button className='btn btn-danger' 
                                onClick={()=>{
                                        Delete(user.user_id)
                                    }}>
                                    Delete
                                </button>
                                
                                </td>

                                <td>
                                <button className='btn btn-info' 
                                onClick={()=>{
                                        Edit(user.user_id)
                                    }}>
                                    Edit
                                </button>
                                
                                </td>
                            </tr>
                                }
                                else
                                {
                                if(user.username.toLowerCase().includes(searchText.toLocaleLowerCase()))
                                {
                                    return <tr>
                                    <td>{user.user_id}</td>
                                    <td>{user.username}</td>
                                    <td>{user.email}</td>
                                    <td>{user.mobileno}</td>
                                    <td>{user.age}</td>
                                    <td>{user.gender}</td>
                                    <td>{user.user_type}</td>
                                    <td>
                                    <button className='btn btn-danger' 
                                    onClick={()=>{
                                            Delete(user.user_id)
                                        }}>
                                        Delete
                                    </button>
                                    
                                    </td>

                                    <td>
                                    <button className='btn btn-info' 
                                    onClick={()=>{
                                            Edit(user.user_id)
                                        }}>
                                        Edit
                                    </button>
                                    
                                    </td>
                                </tr>
                                }
                                }
                            })

                        }
                    </tbody>
                    </table>
                </div>
            </center>
        </>);
    }



export default Profile;