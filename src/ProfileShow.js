function ProfileShow(props)
{
        return (<>
                 <table>
                    <tbody>
                        {/* <tr>
                            <td>ID</td>
                            <td>
                                <input type="number" 
                                        name="bus_id"
                                        value={props.bus.bus_id}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr> */}

                        <tr>
                            <td>Username</td>
                            <td>
                                <input type="text" 
                                        name="username"
                                        value={props.user.username}onChange={props.TextChanged}/>
                            </td>
                        </tr>

                        <tr>
                            <td>Email</td>
                            <td>
                                <input type="text" 
                                        name="email"
                                        value=
                                        {props.user.email}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile.no</td>
                            <td>
                                <input type="number" 
                                        name="capacity"
                                        value=
                                        {props.user.mobileno}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Age</td>
                            <td>
                                <input type="number" 
                                        name="capacity"
                                        value=
                                        {props.user.age}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                                <input type="text" 
                                        name="capacity"
                                        value=
                                        {props.user.gender}
                                        onChange={props.TextChanged}/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <button className='btn btn-success' onClick={props.Insert}>
                                    Add User
                                </button>
                            </td>
                            <td style={{padding: 10}}>
                                
                                <button className='btn btn-warning' onClick={props.Update}>
                                    Update User
                                </button>
                                {"    "}
                                <button className='btn btn-warning' onClick={props.CancelUpdate}>
                                    Cancel User Update
                                </button>
                            </td>
                        </tr>
                    </tbody>
                 </table>
                </>);
}

 
export default ProfileShow;