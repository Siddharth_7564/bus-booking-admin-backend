
    import {useEffect, useState } from 'react';
    import '../node_modules/bootstrap/dist/css/bootstrap.css';
    import RouteAddition from './RouteAddition';
    
    function About() 
    {
        var [routes, setRoutes] = useState([]);
        var [message, setMessage] = useState( "");
        var [route, setRoute] = useState({route_id:0, route_name: "", start_location: "", end_location: "", fare_amount:0});
        var [operation, setOpration] = useState("");
        var [searchText, setSearchText] = useState("");
       
    
        useEffect(()=>{
            Select();
        }, []);
        
        var Select = function()
        {
            debugger;
            var helper = new XMLHttpRequest();
            helper.onreadystatechange = ()=>{
                if(helper.readyState == 4 && helper.status == 200)
                {
                    var result = JSON.parse(helper.responseText);
                    setRoutes(result);
                }
            }
            helper.open("GET", "http://localhost:9999/route");
            helper.send();
        }
    
        var ShowMessage = function(msg)
        {
            setMessage(msg);
            setTimeout(() => 
                            {
                                setMessage("");
                            }, 5000);
                        }
    
        var Delete = function (route_id)
        {
            var helper = new XMLHttpRequest();
            helper.onreadystatechange = ()=>{
                if(helper.readyState == 4 && helper.status == 200)
                {
                    var result = JSON.parse(helper.responseText);
                    if(result.affectedRows > 0)
                    {
                        setMessage("Record removed successfully!!");
                        Select()
                    }
                    
                }
            }
            helper.open("DELETE", "http://localhost:9999/route/" + route_id);
            helper.send();
        }
    
        var TextChanged=function (args)
        {
            debugger;
            var copyOfRoute = {...route};
            copyOfRoute[args.target.name] = args.target.value;
            setRoute(copyOfRoute);
        }
    
        var Insert = function()
        {
            if(operation == "edit")
            {
                setMessage('Complete the Edit Operation First!')
            }
            else
            {
                  // this.state.emp --this holds data to send to server for Insert!
                var helper = new XMLHttpRequest();
                helper.onreadystatechange = ()=>{
                    
                    if(helper.readyState == 4 && helper.status == 200)
                    {
                        debugger;
                        var result = JSON.parse(helper.responseText);
                        if(result.affectedRows > 0)
                        {
                            setMessage("Route Added successfully!!");
                            Select();
                            setRoute({route_id:0, route_name: "", start_location: "", end_location: "", fare_amount:0});
                        }
                        
                    }
                }
                        
            debugger;
            helper.open("POST", "http://localhost:9999/route");
    
            helper.setRequestHeader("Content-Type", "application/json");
    
            var dataToBePassedInStringFormat = JSON.stringify(route);
            helper.send(dataToBePassedInStringFormat);
            }
        }
    
        var Edit= function(route_id){
            for(var i=0; i< routes.length; i++)
            {
                
                if(routes[i].route_id == route_id)
                {
                    var copyOfRoute = {...routes[i]};
                    setRoute(copyOfRoute);
                    setOpration("edit");
                    break;
                }
            }
        }
    
        var Update =function(){
            //Here this.state.emp will have updated record.
            //use this to send data using Put request to the server
    
            var helper = new XMLHttpRequest();
            helper.onreadystatechange = ()=>{
                
                if(helper.readyState == 4 && helper.status == 200)
                {
                    debugger;
                    var result = JSON.parse(helper.responseText);
                    if(result.affectedRows > 0)
                    {
                        setMessage("Record Updated successfully!!");
    
                        Select();
                        
                        setRoute({route_id:0, route_name: "", start_location: "", end_location: "", fare_amount:0})
                    }
                    
                }
            }
    
            debugger;
            helper.open("PUT", "http://localhost:9999/route/"+ route.route_id);
    
            helper.setRequestHeader("Content-Type", "application/json");
    
            var dataToBePassedInStringFormat = JSON.stringify(route);
            helper.send(dataToBePassedInStringFormat);
        }
    
        var CancelUpdate = function()
        {
            setOpration("");
            setRoute({route_id:0, route_name: "", start_location: "", end_location: "", fare_amount:0});
        }
    
        var Search=function(args)
        {
            console.log("U searched " +  args.target.value);
            setSearchText(args.target.value);
        }
        
        return (<>
        
                <center>
                    <RouteAddition route={route} 
                                    TextChanged={TextChanged}
                                    Insert = {Insert}
                                    Update = {Update}
                                    CancelUpdate = {CancelUpdate}/>
                    <h3 className='alert alert-success'>
                        {message}
                    </h3>
                    <div>
                        Search By Name: {" "}
                        <input type='text' value={searchText}
                                onChange={Search}/>
                    </div>
                    <br/>
                    <div className='table-responsive'>
                    
                        <table className='table table-bordered'>
                        <thead>
                            <th>ID</th>
                            <th>Route Name</th>
                            <th>Start Location</th>
                            <th>End Location</th>
                            <th>Fare Amount</th>
                            <th>Delete Here</th>
                        </thead>
                        <tbody>
                            {
                                routes.map((route)=>{
                                    if(searchText=="")
                                    {
                                    return <tr>
                                    <td>{route.route_id}</td>
                                    <td>{route.route_name}</td>
                                    <td>{route.start_location}</td>
                                    <td>{route.end_location}</td>
                                    <td>{route.fare_amount}</td>
                                    <td>
                                    <button className='btn btn-danger' 
                                    onClick={()=>{
                                            Delete(route.route_id)
                                        }}>
                                        Delete
                                    </button>
                                    
                                    </td>
    
                                    <td>
                                    <button className='btn btn-info' 
                                    onClick={()=>{
                                            Edit(route.route_id)
                                        }}>
                                        Edit
                                    </button>
                                    
                                    </td>
                                </tr>
                                    }
                                    else
                                    {
                                    if(route.route_name.toLowerCase().includes(searchText.toLocaleLowerCase()))
                                    {
                                        return <tr>
                                        <td>{route.route_id}</td>
                                        <td>{route.route_name}</td>
                                        <td>{route.start_location}</td>
                                        <td>{route.end_location}</td>
                                        <td>{route.fare_amount}</td>
                                        <td>
                                        <button className='btn btn-danger' 
                                        onClick={()=>{
                                                Delete(route.route_id)
                                            }}>
                                            Delete
                                        </button>
                                        
                                        </td>
    
                                        <td>
                                        <button className='btn btn-info' 
                                        onClick={()=>{
                                                Edit(route.route_id)
                                            }}>
                                            Edit
                                        </button>
                                        
                                        </td>
                                    </tr>
                                    }
                                    }
                                })
    
                            }
                        </tbody>
                        </table>
                    </div>
                </center>
            </>);
        }
     
    

export default About;