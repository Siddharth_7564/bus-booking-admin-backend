import ReactDOM from 'react-dom/client';
import Home from './home';

import Dashboard from './dashboard';
import Test from './test';


import Launcher from './launcher';
import { BrowserRouter } from 'react-router-dom/cjs/react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
//Here we are holding reference to Virtual DOM object of 
//DIV - inside root 
console.log("Inside Index.JS")

// root.render(<Home/>);
// root.render(<Emps></Emps>)
// root.render(<Dashboard/>)

// root.render(<Test/>)
 
// root.render(<Parent/>)

// root.render(<Sample/>)
root.render(<BrowserRouter>
                <Launcher/>
            </BrowserRouter>)