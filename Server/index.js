const express =  require('express');
const config = require('config');


const busRelatedRoutes = require('./routes/bus_copy1');   
const routeRelatedRoutes = require('./routes/route'); 
const userRelatedRoutes = require('./routes/user');


const app = express();

app.use((request, response, next)=>{
    response.setHeader('Access-Control-Allow-Origin',"*");
    response.setHeader('Access-Control-Allow-Headers',"*");
    response.setHeader('Access-Control-Allow-Methods', "*")
    next();
})

app.use(express.json()); 

app.use('/bus',busRelatedRoutes);
app.use('/user',userRelatedRoutes);
app.use('/route',routeRelatedRoutes);

const portNo = config.get("PORT");
app.listen(portNo,()=>{console.log("Server Started at " + portNo)})