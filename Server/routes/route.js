const express =  require('express');
const config = require('config');

const appForRoute = express.Router();
const mysql = require('mysql');
var connection = mysql.createConnection({
    host     : config.get("host"),
    user     :  config.get("user"),
    password :  config.get("password"),
    database :  config.get("database")
   });

appForRoute.get("/", (request, response)=>{
    //response.send("ROUTE GET IS CALLED");
    console.log("ROUTE GET - Request Received...")
    connection.query("select * from route", (error, result)=>{
                if(error==null)
                {
                    var data = JSON.stringify(result) 
                    response.setHeader("Content-Type","application/json");
                    response.write(data);
                } 
                else
                {
                    console.log(error);
                    response.setHeader("Content-Type","application/json");
                    response.write(error)
                }
                response.end();
    })

})

//POST = INSERT INTO DB
appForRoute.post("/", (request, response)=>{
    // console.log("Data Received from Client / Browser / POSTMAN")
    // console.log(request.body)
    // response.send("ROUTE POST IS CALLED");
    console.log("ROUTE POST - Request Received...");
    console.log("Data Received is as below..")
    console.log(request.body)
    var query = 
    `insert into route  (route_name, start_location, end_location, fare_amount) values('${request.body.route_name}', '${request.body.start_location}','${request.body.end_location}','${request.body.fare_amount}')`;

    connection.query(query, (error, result)=>{
        if(error==null)
        {
            var data = JSON.stringify(result) 
            response.setHeader("Content-Type","application/json");
            response.write(data);
        } 
        else
        {
            console.log(error);
            response.setHeader("Content-Type","application/json");
            response.write(error)
        }
        response.end();
})
})




//DELETE  = DELETE FROM DB
appForRoute.delete("/:id", (request, response)=>{
    // response.send("ROUTE DELETE IS CALLED");
    console.log("ROUTE DELETE - Request Received...");
    console.log("Data Received is as below..")
    console.log(request.params)
    var query = 
    `delete from route where route_id = ${request.params.id}`;
                    
    connection.query(query, (error, result)=>{
                        if(error==null)
                        {
                            var data = JSON.stringify(result) 
                            response.setHeader("Content-Type","application/json");
                            response.write(data);
                        } 
                        else
                        {
                            console.log(error);
                            response.setHeader("Content-Type","application/json");
                            response.write(error)
                        }
                        response.end();
                })
})

module.exports = appForRoute;