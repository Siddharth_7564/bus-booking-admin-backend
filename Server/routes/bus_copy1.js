const express =  require('express');
const config = require('config');

const appForBus = express.Router();
const mysql = require('mysql');
var connection = mysql.createConnection({
    host     : config.get("host"),
    user     :  config.get("user"),
    password :  config.get("password"),
    database :  config.get("database")
   });

appForBus.get("/getAllbus", (request, response)=>{
    //response.send("BUS GET IS CALLED");
    console.log("BUS GET - Request Received...")
    connection.query("select * from bus", (error, result)=>{
                if(error==null)
                {
                    var data = JSON.stringify(result) 
                    response.setHeader("Content-Type","application/json");
                    response.write(data);
                } 
                else
                {
                    console.log(error);
                    response.setHeader("Content-Type","application/json");
                    response.write(error)
                }
                response.end();
    })

})

appForBus.get("/busroute", (request, response)=>{
    //response.send("BUS-ROUTE GET IS CALLED");
    console.log("BUS GET - Request Received...")
    connection.query("SELECT bus.bus_name, route.route_name FROM busRoute JOIN bus ON busRoute.bus_id = bus.bus_id JOIN route  ON busRoute.route_id = route.route_id", (error, result)=>{
                if(error==null)
                {
                    var data = JSON.stringify(result) 
                    response.setHeader("Content-Type","application/json");
                    response.write(data);
                } 
                else
                {
                    console.log(error);
                    response.setHeader("Content-Type","application/json");
                    response.write(error)
                }
                response.end();
    })

})

//POST = INSERT INTO DB
appForBus.post("/", (request, response)=>{
    // console.log("Data Received from Client / Browser / POSTMAN")
    // console.log(request.body)
    // response.send("BUS POST IS CALLED");
    console.log("BUS POST - Request Received...");
    console.log("Data Received is as below..")
    console.log(request.body)
    var query = 
    `insert into bus (bus_name, bus_number, capacity) values('${request.body.bus_name}', '${request.body.bus_number}','${request.body.capacity}')`;

    connection.query(query, (error, result)=>{
        if(error==null)
        {
            var data = JSON.stringify(result) 
            response.setHeader("Content-Type","application/json");
            response.write(data);
        } 
        else
        {
            console.log(error);
            response.setHeader("Content-Type","application/json");
            response.write(error)
        }
        response.end();
})
})

appForBus.get("/getBuses", (request, response)=>{
    //response.send("BUS GET IS CALLED");
    console.log("BUS GET - Request Received...")
    var query  = `SELECT Bus.bus_number, Route.route_name, Route.fare_amount FROM Bus JOIN BusRoute ON Bus.bus_id = BusRoute.bus_id JOIN Route ON BusRoute.route_id = Route.route_id WHERE Route.start_location ='${request.body.start_location}' AND Route.end_location = '${request.body.end_location}'`;
    connection.query(query, (error, result)=>{
                if(error==null)
                {
                    var data = JSON.stringify(result) 
                    response.setHeader("Content-Type","application/json");
                    response.write(data);
                } 
                else
                {
                    console.log(error);
                    response.setHeader("Content-Type","application/json");
                    response.write(error)
                }
                response.end();
    })

})


//DELETE  = DELETE FROM DB
appForBus.delete("/:id", (request, response)=>{
    // response.send("EMPS DELETE IS CALLED");
    console.log("BUS DELETE - Request Received...");
    console.log("Data Received is as below..")
    console.log(request.params)
    var query = 
    `delete from bus where bus_id = ${request.params.id}`;
                    
    connection.query(query, (error, result)=>{
                        if(error==null)
                        {
                            var data = JSON.stringify(result) 
                            response.setHeader("Content-Type","application/json");
                            response.write(data);
                        } 
                        else
                        {
                            console.log(error);
                            response.setHeader("Content-Type","application/json");
                            response.write(error)
                        }
                        response.end();
                })
})

module.exports = appForBus;


