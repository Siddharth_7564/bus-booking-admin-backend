const express =  require('express');
const config = require('config');

const appForUser = express.Router();
const mysql = require('mysql');
var connection = mysql.createConnection({
    host     : config.get("host"),
    user     :  config.get("user"),
    password :  config.get("password"),
    database :  config.get("database")
   });


//GET = SELECT FROM DB
appForUser.get("/", (request, response)=>{
    //response.send("EMPS GET IS CALLED");
    console.log("User GET - Request Received...")
    connection.query("select * from user", (error, result)=>{
                if(error==null)
                {
                    var data = JSON.stringify(result) 
                    response.setHeader("Content-Type","application/json");
                    response.write(data);
                } 
                else
                {
                    console.log(error);
                    response.setHeader("Content-Type","application/json");
                    response.write(error)
                }
                response.end();
    })

})

//POST = INSERT INTO DB
appForUser.post("/register", (request, response)=>{
    // console.log("Data Received from Client / Browser / POSTMAN")
    // console.log(request.body)
    // response.send("EMPS POST IS CALLED");
    console.log("User POST - Request Received...");
    console.log("Data Received is as below..")
    console.log(request.body)
    var query = 
    `insert into users values(${request.body.id}, '${request.body.name}','${request.body.email}','${request.body.phone}','${request.body.address}','${request.body.password}','${request.body.role}','${request.body.reg_date}','${request.body.last_activity}')`;

    connection.query(query, (error, result)=>{
        if(error==null)
        {
            var data = JSON.stringify(result) 
            response.setHeader("Content-Type","application/json");
            response.write(data);
        } 
        else
        {
            console.log(error);
            response.setHeader("Content-Type","application/json");
            response.write(error)
        }
        response.end();
})
})

appForUser.get("/:id",(request,response)=>{

    console.log("USER GET BY ID IS CALLED")
    var query = `select * from users where id = ${request.params.id}`
    connection.query(query,(error,  result)=>{
        if(error == null){

            var data = JSON.stringify(result)
            response.setHeader("Content-Type","application/json");
            response.write(data)
        }
        else
        {
            console.log(error);
            response.setHeader("Content-Type","application/json")
            response.write(error)
        }
        response.end();
    })

})

// //PUT = UPDATE INTO DB
// appForEmps.put("/:ENo", (request, response)=>{
//     //response.send("EMPS PUT IS CALLED");
//     console.log("EMPS PUT - Request Received...");
//     console.log("Data Received is as below..")
//     console.log(request.body)
//     console.log(request.params)

//     var query = 
//     `update Emp set EName = '${request.body.EName}',
//                     EAddress = '${request.body.EAddress}' where ENo = ${request.params.ENo}`;

//     connection.query(query, (error, result)=>{
//                         if(error==null)
//                         {
//                             var data = JSON.stringify(result) 
//                             response.setHeader("Content-Type","application/json");
//                             response.write(data);
//                         } 
//                         else
//                         {
//                             console.log(error);
//                             response.setHeader("Content-Type","application/json");
//                             response.write(error)
//                         }
//                         response.end();
//                 })
// })

// //DELETE  = DELETE FROM DB
// appForEmps.delete("/:ENo", (request, response)=>{
//     // response.send("EMPS DELETE IS CALLED");
//     console.log("EMPS DELETE - Request Received...");
//     console.log("Data Received is as below..")
//     console.log(request.params)
//     var query = 
//     `delete from Emp where ENo = ${request.params.ENo}`;
                    
//     connection.query(query, (error, result)=>{
//                         if(error==null)
//                         {
//                             var data = JSON.stringify(result) 
//                             response.setHeader("Content-Type","application/json");
//                             response.write(data);
//                         } 
//                         else
//                         {
//                             console.log(error);
//                             response.setHeader("Content-Type","application/json");
//                             response.write(error)
//                         }
//                         response.end();
//                 })
// })

module.exports = appForUser;