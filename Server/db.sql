 create database punedac

 use punedac;
 create table Emp 
 (ENo int primary key,
 	EName varchar(50),
 	EAddress varchar(50));

use punedac;
 insert into Emp values(1, 'Mahendra', 'Pune');
 insert into Emp values(2, 'Virat', 'Nagpur');
 insert into Emp values(3, 'Sourav', 'Panji');
 insert into Emp values(4, 'Ravindra', 'Mumbai');
 insert into Emp values(5, 'Rohit', 'Delhi');


select * from Emp;